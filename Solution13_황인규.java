package com.ssafy.algo;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Scanner;

public class Solution13_황인규 {

	public static void main(String[] args) throws FileNotFoundException {
		// TODO Auto-generated method stub
		System.setIn(new FileInputStream("res/fr_01.txt"));
		Scanner sc = new Scanner(System.in);
		int testCase = sc.nextInt();
		int cnt =0;
		while(testCase>0) {
			cnt+=1;
			testCase = testCase -1;
		int N=sc.nextInt();
		char[][] map = new char[N][N];
		int [][] copyMap = new int[N][N];
		for(int i=0; i<N; i++) {
			for(int j=0; j<N; j++) {
				String s =sc.next();
				map[i][j] = s.charAt(0);
			}
		}
		
		//System.out.println(Arrays.deepToString(copyMap));
		
		//System.out.println(map[0][1]);
		//System.out.println(Arrays.deepToString(map));
		//방문과 완전탐색
		
		
		
		int dx[] = {0,1,0,-1,1,-1,1,-1};
		int dy[] = {1,0,-1,0,-1,1,1,-1};
		
		boolean flag = false;
		int max = -987654321;
	
		int ans = 0;
		for(int i=0; i<N; i++) {
			for(int j=0; j<N; j++) {
				for(int k=0; k<8; k++) {
					
					int ddx = i + dx[k];
					int ddy = j + dy[k];
					
					if(ddx >=0 && ddx<N && ddy>=0 && ddy<N) {
						if(map[ddx][ddy] == 'G') {
							if(max<2)
								max = 2;
							flag = true;
							break;
						}
					}
				}
				//System.out.println(Arrays.deepToString(copyMap));
				if(flag == false) {
					
					int rowSum=0, columnSum=0, temp;
					for(int u=0; u<N; u++) {
						if(map[u][j] == 'B')rowSum+=1;
						if(map[i][u] == 'B')columnSum+=1;
						
					}
					temp = rowSum + columnSum-1;
					if(max<temp)
						max=temp;
					
				}
				flag = false;
				
		}
		ans = max;	
	}
		System.out.println("#" + cnt +" "+ans);
	}
}
}
