package com.ssafy.algo;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Scanner;

public class Solution22_황인규 {

	public static void main(String[] args) throws FileNotFoundException {
		// TODO Auto-generated method stub
		
		System.setIn(new FileInputStream("res/fr_02.txt"));
		Scanner sc = new Scanner(System.in);
		int testCase=sc.nextInt();
		int cnt = 0;
		while(testCase>0) {
			cnt+=1;
			testCase-=1;
		int N=sc.nextInt();
		int count = sc.nextInt();
		//System.out.println("N : "+N+" count : "+count);
		int map[][] = new int[N][N];
		
		
		
		int sur = count; //소금쟁이 수
		
		int jumpingZero[] = {0,0,0};
		int jumpingPosivieValue[] = {1,2,3};
		int jumpingNegativeValue[] = {-1,-2,-3};
		for(int i=0; i<count; i++) {
			int x = sc.nextInt();
			int y = sc.nextInt();
			int dir = sc.nextInt();
			
			switch(dir) {
			case 1: //상
				map[x][y]=1;
				for(int j=0; j<3; j++) {
					int dx = x +jumpingPosivieValue[j];
					int dy = y +jumpingZero[j];
					if((dx>=0 && dx<N && dy>=0 && dy<N) && map[dx][dy] != 1) {
						map[dx][dy] =1;
						map[x][y]=0;
					}else {
						sur-=1;
						break;
					}
				}
				break;
			case 2: //하
				map[x][y]=1;
				for(int j=0; j<3; j++) {
					int dx = x +jumpingNegativeValue[j];
					int dy = y +jumpingZero[j];
					if((dx>=0 && dx<N && dy>=0 && dy<N) && map[dx][dy] != 1) {
						map[dx][dy] =1;
						map[x][y]=0;
					}else {
						sur-=1;
						break;
					}
				}
				break;
			case 3: //좌
				map[x][y]=1;
				for(int j=0; j<3; j++) {
					int dx = x +jumpingZero[j];
					int dy = y +jumpingNegativeValue[j];
					if((dx>=0 && dx<N && dy>=0 && dy<N) && map[dx][dy] != 1) {
						map[dx][dy] =1;
						map[x][y]=0;
					}else {
						sur-=1;
						break;
					}
				}
				break;
			case 4: //우
				map[x][y]=1;
				for(int j=0; j<3; j++) {
					int dx = x +jumpingZero[j];
					int dy = y +jumpingPosivieValue[j];
					if((dx>=0 && dx<N && dy>=0 && dy<N) && map[dx][dy] != 1) {
						map[dx][dy] =1;
						map[x][y]=0;
						//System.out.println(Arrays.deepToString(map));
					}else {
						sur-=1;
						break;
					}
				}
				break;
			default :
				break;
			}
		}
		System.out.println("#"+cnt+" "+sur);
	}
  }
}
