package com.ssafy.algo;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Scanner;

public class Solution32_황인규 {

	public static void main(String[] args) throws FileNotFoundException {
		// TODO Auto-generated method stub
		System.setIn(new FileInputStream("res/fr_03.txt"));
		Scanner sc = new Scanner(System.in);
		
		int cnt=0;
		int testCase = sc.nextInt();
		while(testCase>0) {
		cnt+=1;
		testCase = testCase-1;
		int scaleY=sc.nextInt();
		int scaleX=sc.nextInt(); //map 크기
		int participant = sc.nextInt(); //참가자수
		
		sc.nextLine();
		
		
		int map[][] = new int[scaleY][scaleX];
		
		for(int i=0; i<scaleY; i++) {
			for(int j=0; j<scaleX; j++) {
				map[i][j] = sc.nextInt();
			}
		}
		//System.out.println(Arrays.deepToString(map));
		
		
		int[][] participantArray = new int[participant][3];
		for(int i=0; i<participant; i++) {
			int participantX =sc.nextInt();
			participantArray[i][0] = participantX;
			int participantY =sc.nextInt();
			participantArray[i][1] = participantY;
			int jumpingCnt = sc.nextInt();
			participantArray[i][2] = jumpingCnt;
		}
		
		int trickCnt = sc.nextInt();
		for(int i=0; i<trickCnt; i++) {
			int trickX = sc.nextInt();
			int trickY = sc.nextInt();
			for(int j=0; j<scaleY; j++) {
				for(int k=0; k<scaleX; k++) {
					if(map[trickX-1][trickY-1]>0)
						map[trickX-1][trickY-1] = 0;
				}
			}
		}
		
		//System.out.println(Arrays.deepToString(map));
		
		int ans = 0;
		int prizeCnt=0;
		//boolean flag = false;
		for(int i=0; i<participant; i++) {
			prizeCnt+=1;
			ans+=-1000;
			//System.out.println(mapValue);
			while(true) {
			boolean flag = false;	
			int mapValue = map[participantArray[i][0]-1][participantArray[i][1]-1];
			
			int quotient = mapValue/10; //몫은 방향 
			int remainder = mapValue%10; //나머지는 점프
			participantArray[i][2] -= 1; //JumpingCnt 차감
			
			
			switch(quotient) {
			case 1: //우
				participantArray[i][0] +=0;
				participantArray[i][1] +=remainder;
				if(map[participantArray[i][0]-1][participantArray[i][1]-1]==0 || participantArray[i][1]>scaleX)
				{
					flag = true;
				}
				break;
			case 2: //아래
				participantArray[i][0] +=remainder;
				participantArray[i][1] +=0;
				if(map[participantArray[i][0]-1][participantArray[i][1]-1]==0 || participantArray[i][0]>scaleY)
				{
					flag = true;
				}
				break;
			case 3: //좌
				participantArray[i][0] +=0;
				participantArray[i][1] -=remainder;
				if(participantArray[i][0]-1 >=0) {
				if(map[participantArray[i][0]-1][participantArray[i][1]-1]==0 || participantArray[i][1]<0)
				{
					flag = true;
				}
				}else{
					flag = true;
				}
				break;
			case 4: //위
				
				participantArray[i][0] -=remainder;
				participantArray[i][1] +=0;
				
				
				if(participantArray[i][0] -1 >=0) {
					if(map[participantArray[i][0]-1][participantArray[i][1]-1]==0 || participantArray[i][0]<0)
					{
					flag = true;
					}
				}else {
					flag = true;
				}
				break;
			default :
				break;
			
						
				}

			if(flag == true) {
				break;
			}
			
			if(participantArray[i][2]==0) {
				int value = map[participantArray[i][0]-1][participantArray[i][1]-1];
				ans = ans + value*100;
				break;
			}
			
			}	
		}
		System.out.println("#"+cnt+" "+ans);
	}

		
	}
}
